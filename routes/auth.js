const express = require('express')
const passport = require('passport')
const router = express.Router()
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const User = require('../models/User')

// @desc    Auth with Google
// @route   GET /auth/google
router.get('/google', passport.authenticate('google', { scope: ['profile'] }))

// @desc    Google auth callback
// @route   GET /auth/google/callback
router.get('/google/callback', 
  passport.authenticate('google', { failureRedirect: '/dashboard' }),
  function(req, res) {
    // Successful authentication, redirect dashboard.
    console.log('request from passport: ',req);
    console.log('response from passport: ',res);
    res.redirect('/dashboard');
  });

// @desc    Logout user
// @route   /auth/logout
router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

module.exports = router